type PercentageOptions = {
  round: boolean | number;
  withVisual: boolean;
};
