type ParserOptions = {
  pathToFile: string;
  filters: Filter[];
};

type Filter<Ctx extends Record<string, unknown>, Out extends Record<string, unknown>> = {
  context: Ctx;
  handler: Handler<Ctx>;
  postHandler: PostHandler<Ctx, Out>;
  serializer: SerializerHandler<Out>;
};

type Handler<Context> = (ctx: Context, message: Message, messages: Message[]) => void;

type PostHandler<Context, OutputItem> = (ctx: Context, messages: Message[]) => OutputItem[];

type SerializerHandler<OutputItem> = (stats: OutputItem[]) => void;

type Chat = {
  name: string;
  type: string;
  id: number;
  messages: Message[];
};

type Message = {
  id: number;
  type: string;
  date: string;
  date_unixtime: string;
  from: string;
  from_id: string;
  text: string;
  text_entities: Record<string, unknown>[];
};
