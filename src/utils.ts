export function percentage(
  part: number,
  whole: number,
  options: PercentageOptions,
): number | string {
  let percents: string | number = part * 100 / whole;

  if (options.round != null) {
    const digits = 10 ** (typeof options.round === "number" ? options.round : 2);
    percents = Math.round(percents * digits) / digits;
  }

  if (options.withVisual) {
    percents = String(percents).padStart(5, " ") + "% " + `\u2588`.repeat(percents * 4);
  }

  return percents;
}
