export async function parseTelegramStats(options: ParserOptions) {
  const messages = await fetchMessagesFromFile(options.pathToFile);
  runFilters(messages, options.filters);
}

async function fetchMessagesFromFile(
  pathToFile: string | URL,
): Promise<Message[]> {
  try {
    const decoder = new TextDecoder("utf-8");
    const data = await Deno.readFile(pathToFile);
    const text = decoder.decode(data);

    return JSON.parse(text).messages;
  } catch (error) {
    const errorMessage = "An error happen during importing result file";
    throw new Error(errorMessage, { cause: error });
  }
}

function runFilters<C extends Record<string, unknown>, O extends Record<string, unknown>>(
  messages: Message[],
  filters: Filter<C, O>[],
) {
  messages.forEach((message) => filters.forEach((filter) => filter.handler(filter.context, message, messages)));
  const allStats = filters.map((filter) => filter.postHandler(filter.context, messages));
  allStats.forEach((output, index) => filters[index].serializer(output));
}
