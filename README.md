### Example

```bash
$ deno task example <path to result.json with Telegram stats>
```
### Usage

1. Create types for your filter

```typescript
type ContextUser = {
  id: string;
  user: string;
  messages: number;
  symbols: number;
};

type OutputUser = ContextUser & { symbolsPerMessage: number };

type FilterContext = { users: Record<string, ContextUser> };
```

2. Create a `Filter`

```typescript
const someFilter: Filter<FilterContext, OutputUser> = {
  context: { words: {} },                       // set a starting context
  handler: (context, message) => {},            // calculate context per message
  postHandler: (context, messages) => {},       // do some final calculations based on context
  serializer: (output) => {},                   // print results or save them
};
```

3. Call the function will configuration

```typescript
parseTelegramStats({
  pathToFile: Deno.args[0],                     // process.argv[2] equivalent
  filters: [someFilter],
});
```

4. Run file with `parseTelegramStats`

```bash
$ deno run --allow-read <path to .ts file> <path to .json file>
```
