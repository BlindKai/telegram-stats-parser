import { parseTelegramStats } from "../src/parser.ts";
import { percentage } from "../src/utils.ts";

type ContextUser = {
  id: string;
  user: string;
  messages: number;
  symbols: number;
};

type OutputUser = ContextUser & { symbolsPerMessage: number };

type FilterContext = { users: Record<string, ContextUser> };

const someFilter: Filter<FilterContext, OutputUser> = {
  context: { users: {} },
  handler: (context, message) => {
    const { from_id, from, text } = message;

    if (context.users[from_id]) {
      context.users[from_id].messages++;
      context.users[from_id].symbols += text.length;

      return;
    }

    context.users[from_id] = {
      id: from_id,
      user: from?.replace(/[^a-zA-Z0-9а-яА-Я]+/g, "").trim(),
      messages: 1,
      symbols: text.length,
    };
  },
  postHandler: (context, messages) => {
    const usersStat = Object.values(context.users)
      .map((s) => ({
        ...s,
        symbolsPerMessage: +(s.symbols / s.messages).toFixed(2),
      })).sort((a, b) => b.symbols - a.symbols);

    const allSymbols = usersStat.reduce((acc, item) => acc += item.symbols, 0);

    return usersStat.map((u) => ({
      ...u,
      symbolsPercentage: percentage(u.symbols, allSymbols, { round: true, withVisual: true }),
    }));
  },
  serializer: (output) => {
    console.table(output);
  },
};

parseTelegramStats({
  pathToFile: Deno.args[0],
  filters: [someFilter],
});
