import { parseTelegramStats } from "../src/parser.ts";
import { percentage } from "../src/utils.ts";

type ContextUser = {
  id: string;
  user: string;
  messages: number;
  symbols: number;
};

type FilterContext = { words: string[]; stats: Record<string, number> };

type FilterOutput = { word: string; count: number; percent: string | number };

const words = ["Банан", "Ананас", "Яблуко", "Вишня", "Сметана"];

const someFilter: Filter<FilterContext, FilterOutput> = {
  context: {
    words,
    stats: words.reduce<Record<string, number>>((acc, w) => (acc[w] = 0, acc), {}),
  },
  handler: (context, message) => {
    const { text } = message;

    context.words.forEach((w) => {
      if (String(text).toLowerCase().includes(w.toLowerCase())) context.stats[w]++;
    });
  },
  postHandler: (context) => {
    const sum = Object.values(context.stats).reduce((acc, i) => (acc += i, acc), 0);

    return Object.entries(context.stats).map(([key, value]) => ({
      word: key,
      count: value,
      percent: percentage(value, sum, { round: 2, withVisual: false }),
    }));
  },
  serializer: (output) => {
    console.table(output);
  },
};

parseTelegramStats({
  pathToFile: Deno.args[0],
  filters: [someFilter],
});
